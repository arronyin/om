package com.zero2oneit.mall.goods.service.impl;

import com.zero2oneit.mall.common.bean.goods.GoodsProductInfo;
import com.zero2oneit.mall.common.bean.goods.GoodsProductInfoRelation;
import com.zero2oneit.mall.common.bean.goods.Products;
import com.zero2oneit.mall.common.query.goods.GoodsProductInfoQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import com.zero2oneit.mall.common.utils.snowflake.IdWorker;
import com.zero2oneit.mall.common.utils.snowflake.IdWorkerFactory;
import com.zero2oneit.mall.goods.mapper.GoodsProductInfoMapper;
import com.zero2oneit.mall.goods.service.GoodsProductInfoRelationService;
import com.zero2oneit.mall.goods.service.GoodsProductInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-02-22
 */
@Service
public class GoodsProductInfoServiceImpl extends ServiceImpl<GoodsProductInfoMapper, GoodsProductInfo> implements GoodsProductInfoService {

    @Autowired
    private GoodsProductInfoMapper goodsProductInfoMapper;

    @Autowired
    private GoodsProductInfoRelationService goodsRelationService;

    @Override
    public BoostrapDataGrid pageList(GoodsProductInfoQueryObject qo) {
        int total = goodsProductInfoMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total ==0 ? Collections.EMPTY_LIST : goodsProductInfoMapper.selectAll(qo));
    }

    @Override
    public void status(String id, Integer status) {
        goodsProductInfoMapper.status(id, status);
    }

    @Override
    public void audit(String ids, Integer productStatus, String approveResult, String userName) {
        goodsProductInfoMapper.audit(ids, productStatus, approveResult, userName);
    }

    @Override
    public Map<String, Object> load(String id) {
        return goodsProductInfoMapper.load(id);
    }

    @Override
    public void copy(String id) {
        IdWorker idworker = IdWorkerFactory.create(0,0);
        long nextId = idworker.nextId();
        goodsProductInfoMapper.copyGoods(nextId, id);
        goodsProductInfoMapper.copyGoodsRelation(nextId, id);
    }

    @Override
    public List<Products> esList(String ids) {
        return goodsProductInfoMapper.esList(ids);
    }

    @Override
    public void bind(String productIds, String ruleId, String moudleId) {
        goodsProductInfoMapper.bindSecKill(productIds, ruleId, moudleId);
    }

    @Override
    public void unBind(String productIds) {
        goodsProductInfoMapper.unSeckill(productIds);
    }

    @Override
    public void down(String ids, Integer status) {
        goodsProductInfoMapper.down(ids, status);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public R addOrEdit(GoodsProductInfo goods) {
        GoodsProductInfoRelation relation = null;
        for (int i = 0; i < goods.getMarketPrice().split(",").length; i++) {
            if(goods.getId() == null){
                //生成分布式商品ID 保存商品表
                IdWorker idworker = IdWorkerFactory.create(0,0);
                goods.setId(idworker.nextId());
                goodsProductInfoMapper.insert(goods );
                relation = getGoodsProductInfoRelation(goods, i);
                goodsRelationService.save(relation);
                goods.setId(null);
            }else{
                goods.setProductStatus(0);
                goodsProductInfoMapper.updateById(goods);
                relation = getGoodsProductInfoRelation(goods, i);
                goodsRelationService.updateById(relation);
            }
        }
        return R.ok();
    }

    private GoodsProductInfoRelation getGoodsProductInfoRelation(GoodsProductInfo goods, int i) {
        GoodsProductInfoRelation relation;
        relation = new GoodsProductInfoRelation();
        relation.setId(goods.getId());
        relation.setProductName(goods.getProductName());
        relation.setMarketPrice(new BigDecimal(goods.getMarketPrice().split(",")[i]));
        relation.setSupplyPrice(new BigDecimal(goods.getSupplyPrice().split(",")[i]));
        relation.setProductPrice(new BigDecimal(goods.getProductPrice().split(",")[i]));
        relation.setProductStock(Integer.valueOf(goods.getProductStock().split(",")[i]));
        relation.setSaleAreaName(goods.getSaleAreaName().split(",")[i]);
        relation.setSaleAmount(Integer.valueOf(goods.getSaleAmount().split(",")[i]));
        return relation;
    }

}