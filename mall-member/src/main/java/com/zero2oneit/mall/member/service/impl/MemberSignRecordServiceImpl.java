package com.zero2oneit.mall.member.service.impl;

import com.zero2oneit.mall.common.bean.member.MemberSignRecord;
import com.zero2oneit.mall.common.query.member.MemberSignRecordQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zero2oneit.mall.member.mapper.MemberSignRecordMapper;
import com.zero2oneit.mall.member.service.MemberSignRecordService;

import java.util.Collections;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-13
 */
@Service
public class MemberSignRecordServiceImpl extends ServiceImpl<MemberSignRecordMapper, MemberSignRecord> implements MemberSignRecordService {

    @Autowired
    private MemberSignRecordMapper memberSignRecordMapper;

    @Override
    public BoostrapDataGrid recordList(MemberSignRecordQueryObject qo) {
        int total = memberSignRecordMapper.selectTotal(qo);
        return new BoostrapDataGrid(total, total ==0 ? Collections.EMPTY_LIST : memberSignRecordMapper.selectAll(qo));
    }

    @Override
    public BoostrapDataGrid recordLists(MemberSignRecordQueryObject qo) {
        int total = memberSignRecordMapper.selectTotals(qo);
        return new BoostrapDataGrid(total, total ==0 ? Collections.EMPTY_LIST : memberSignRecordMapper.selectAlls(qo));
    }


}