package com.zero2oneit.mall.member.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.PrizeWin;
import com.zero2oneit.mall.common.query.member.PrizeWinQueryObject;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @author Tg
 * @email zero2oneit@163.com
 * @date 2021-05-25
 */
public interface PrizeWinService extends IService<PrizeWin> {

    BoostrapDataGrid pageList(PrizeWinQueryObject qo);

    List<HashMap<String, Object>> selectList(String memberId);

}

