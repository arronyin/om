package com.zero2oneit.mall.feign.member;

import com.zero2oneit.mall.common.bean.member.MemberSignRule;
import com.zero2oneit.mall.common.query.member.MemberSignQueryObject;
import com.zero2oneit.mall.common.query.member.MemberSignRecordQueryObject;
import com.zero2oneit.mall.common.query.member.MemberSignRuleQueryObject;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Description:
 *
 * @author Cmx
 * @date 2021/5/12 19:27
 */
@FeignClient("member-service")
public interface MemberSignFeign {

    @PostMapping("/admin/member/sign/ruleList")
    BoostrapDataGrid ruleList(@RequestBody MemberSignRuleQueryObject qo);

    @PostMapping("/admin/member/sign/ruleAddOrEdit")
    R addOrEdit(@RequestBody MemberSignRule memberSignRule);

    @PostMapping("/admin/member/sign/recordList")
    BoostrapDataGrid recordList(MemberSignRecordQueryObject qo);

    @PostMapping("/admin/member/sign/signList")
    BoostrapDataGrid signList(MemberSignQueryObject qo);
}
